#include "defines.h"
#include "GLSLShader.h"
#include "TeapotVboPatch.h"

GLFWwindow *window = NULL;

GLSLShader *teapotTessellationShader = NULL;

TeapotVboPatch *teapot = NULL;

int winWidth = 960;
int winHeight = 540;


glm::mat4 ModelviewMatrix = glm::mat4(1.0f);
glm::mat4 ProjectionMatrix = glm::mat4(1.0f);

glm::mat4 ModelMatrix = glm::mat4(1.0f);
glm::mat4 ViewMatrix = glm::mat4(1.0f);

GLfloat inner = 1.0f;
GLfloat outer = 1.0f;

GLfloat zoom = 2.f;

GLint maxPatchVertices = 0;

GLenum renderingMode = GL_LINE;

static void error_callback(int error,const char* description);
static void framebuffer_size_callback(GLFWwindow *window,int width,int height);
static void scroll_callback(GLFWwindow *window,double x,double y);
static void key_callback(GLFWwindow *window,int key,int scancode,int action,int mods);

void startup();
void shutdown();
void render(float currentTime);
void loadShaders();

void startup()
{

    //load the shader
    loadShaders();

    glClearColor(0.5f,0.5f,0.5f,1.0);

    //create the vbo teapot patch
    teapot = new TeapotVboPatch(teapotTessellationShader);

    ProjectionMatrix = glm::perspective(30.0f,(GLfloat)winWidth/winHeight,0.3f,100.0f);

    glViewport(0,0,winWidth,winHeight);

    //get the maximum patch vertices that this driver supports
    glGetIntegerv(GL_MAX_PATCH_VERTICES,&maxPatchVertices);

    glCullFace(GL_BACK);

}

void shutdown()
{
    if(teapotTessellationShader)
    {
        teapotTessellationShader->DeleteShaderProgram();
        delete teapotTessellationShader;
        teapotTessellationShader = NULL;
    }

    if(teapot)
    {
        delete teapot;
        teapot = NULL;
    }
}

void render(float currentTime)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glm::mat4 T1             = glm::translate(glm::mat4(1.0f),glm::vec3(0.0f,0.0f,-zoom));
    glm::mat4 T2             = glm::translate(glm::mat4(1.0f),glm::vec3(0.0f,0.4,0.0f));
    glm::mat4 Rx	         = glm::rotate(glm::mat4(1.0),10.f, glm::vec3(1.0f, 0.0f, 0.0f));
    glm::mat4 Ry             = glm::rotate(glm::mat4(1.0f),currentTime,glm::vec3(0.0f,1.0f,0.0f));


    ModelMatrix	             =  T1 * Rx * T2 * Ry;


    ModelviewMatrix =  ViewMatrix *  ModelMatrix;


    teapotTessellationShader->Use();

    glUniformMatrix4fv((*teapotTessellationShader)("ModelviewMatrix"),1,GL_FALSE,glm::value_ptr(ModelviewMatrix));

    glUniformMatrix4fv((*teapotTessellationShader)("ProjectionMatrix"),1,GL_FALSE,glm::value_ptr(ProjectionMatrix));

    glUniform1f((*teapotTessellationShader)("inner"),inner);
    glUniform1f((*teapotTessellationShader)("outer"),outer);

    glPolygonMode(GL_FRONT_AND_BACK,renderingMode);

    teapot->render(teapotTessellationShader);


    teapotTessellationShader->UnUse();
}


int main()
{
    // Initialise GLFW before using any of its function
    if( !glfwInit() )
    {
       std::cerr << "Failed to initialize GLFW." << std::endl;
       exit(EXIT_FAILURE);
    }

    glfwWindowHint(GLFW_SAMPLES, 4);
    //we want the opengl 4
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 4);
    //we do not want the old opengl
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    // Open a window and create its OpenGL context
    window = glfwCreateWindow( winWidth, winHeight, "Teapot Tessellation", NULL, NULL);

    if( window == NULL )
    {
       fprintf( stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 4.3 compatible. Try the 2.1 version of the tutorials.\n" );

       //we could not initialize the glfw window
       //so we terminate the glfw
       glfwTerminate();
       exit(EXIT_FAILURE);
    }

    glfwSetErrorCallback(error_callback);
    glfwSetFramebufferSizeCallback(window,framebuffer_size_callback);
    glfwSetScrollCallback(window,scroll_callback);
    glfwSetKeyCallback(window,key_callback);


    //make the current window context the current one
    glfwMakeContextCurrent(window);
    glfwSwapInterval(1);

    glfwGetFramebufferSize(window,&winWidth,&winHeight);
    framebuffer_size_callback(window,winWidth,winHeight);

    //initialize glew
    //needed for the core profile
    glewExperimental  = true;

    if(glewInit() != GLEW_OK)
    {
        std::cerr << "Failed to initialize GLEW" << std::endl;
        exit(EXIT_FAILURE);
    }

    if(!glewIsSupported("GL_VERSION_4_4"))
    {
        std::cerr << "OpenGL version 4.4 is yet to be supported" << std::endl;
        exit(EXIT_FAILURE);
    }

    while(glGetError() != GL_NO_ERROR) {}

    std::cout << "**************** OpenGL Driver Information *********************" << std::endl;

    std::cout << "OpenGL version: " << glGetString(GL_VERSION) << std::endl;
    std::cout << "GLSL version: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << std::endl;
    std::cout << "GLEW version: " << glewGetString(GLEW_VERSION) << std::endl;
    std::cout << "OpenGL vendor: " << glGetString(GL_VENDOR) << std::endl;
    std::cout << "Renderer: " << glGetString(GL_RENDERER) << std::endl;

    std::cout << "****************************************************************" << std::endl;

    //call some initialization function
    startup();


    do
    {
        render((float)glfwGetTime());
        glfwSwapBuffers(window);
        glfwPollEvents();
    } while(glfwGetKey(window,GLFW_KEY_ESCAPE) != GLFW_PRESS &&
            glfwWindowShouldClose(window) == 0);

    //once the following function is called
    //no more events will be dilivered for that
    //window and its handle becomes invalid
    glfwDestroyWindow(window);

    // Close OpenGL window and terminate GLFW
    glfwTerminate();

    //release all the resources that are allocated
    shutdown();

    exit(EXIT_SUCCESS);

}


void error_callback(int error,const char* description)
{

}

void framebuffer_size_callback(GLFWwindow *window,int width,int height)
{
    if(height < 1)
        height = 1;

    winWidth = width;
    winHeight = height;

    glViewport(0,0,winWidth,winHeight);

    ProjectionMatrix = glm::perspective(30.0f,(GLfloat)winWidth/winHeight,0.3f,100.0f);
}

void scroll_callback(GLFWwindow *window,double x,double y)
{
    //increase/decrease the field of view
    zoom += (float)y/4.f;

    if(zoom < 0)
        zoom = 0;
}

void key_callback(GLFWwindow *window,int key,int scancode,int action,int mods)
{
    if(action != GLFW_PRESS) return;

    switch(key)
    {
        case GLFW_KEY_UP:
            outer = outer < maxPatchVertices ? outer + 1 : 1;
            break;
        case GLFW_KEY_DOWN:
            outer = outer > 1 ? outer -1 : 1;
            break;
        case GLFW_KEY_RIGHT:
            inner = inner < maxPatchVertices ? inner + 1 : 1;
            break;
        case GLFW_KEY_LEFT:
            inner = inner > 1 ? inner -1 : 1;
            break;
        case GLFW_KEY_F:
            renderingMode = ( renderingMode == GL_FILL ? GL_LINE : GL_FILL);
            break;
        case GLFW_KEY_R:
            outer = 1;
            inner = 1;
            break;
        default:
            break;
    }
}


void loadShaders()
{
    if(teapotTessellationShader)
    {
        teapotTessellationShader->DeleteShaderProgram();
        delete teapotTessellationShader;
        teapotTessellationShader = NULL;
    }

    teapotTessellationShader = new GLSLShader();

    teapotTessellationShader->LoadFromFile(GL_VERTEX_SHADER,"shaders/teapottess.vert");
    teapotTessellationShader->LoadFromFile(GL_TESS_CONTROL_SHADER,"shaders/teapottess.tcs");
    teapotTessellationShader->LoadFromFile(GL_TESS_EVALUATION_SHADER,"shaders/teapottess.tes");
    teapotTessellationShader->LoadFromFile(GL_FRAGMENT_SHADER,"shaders/teapottess.frag");


    teapotTessellationShader->CreateAndLinkProgram();

    teapotTessellationShader->Use();

    teapotTessellationShader->AddUniform("ProjectionMatrix");
    teapotTessellationShader->AddUniform("ModelviewMatrix");
    teapotTessellationShader->AddUniform("inner");
    teapotTessellationShader->AddUniform("outer");

    teapotTessellationShader->UnUse();
}



